package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.detail(id);

		request.setAttribute("id", user.getId());
		request.setAttribute("loginId", user.getLoginId());
		request.setAttribute("name", user.getName());
		request.setAttribute("date", user.getBirthDate());


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 request.setCharacterEncoding("UTF-8");


			String name = request.getParameter("name");
			String date = request.getParameter("date");
			String password =request.getParameter("password");
			String pasword_c=request.getParameter("password_c");
			String id =request.getParameter("id");
			String loginId =request.getParameter("loginId");
			if (!password.equals(pasword_c)) {

				request.setAttribute("errMsg", "更新に失敗しました。");
				request.setAttribute("id",id);
				request.setAttribute("loginId", loginId);
				request.setAttribute("name", name);
				request.setAttribute("date", date);


				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
				dispatcher.forward(request, response);
				return;
			}

			if (name.equals("")|| date.equals("")) {

				request.setAttribute("errMsg", "更新に失敗しました。");
				request.setAttribute("id",id);
				request.setAttribute("loginId", loginId);
				request.setAttribute("name", name);
				request.setAttribute("date", date);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
				dispatcher.forward(request, response);
				return;
			}

			UserDao userDao = new UserDao();

			if(password.equals("")&&pasword_c.equals("")) {
				userDao.UpdateNotPassword(name,date,id);
			}else {
				String result= userDao.Cipher(password);

				userDao.Update(name,date,result,id);
			}

			response.sendRedirect("UserListServlet");
	}
}