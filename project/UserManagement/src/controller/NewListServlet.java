package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class NewListServlet
 */
@WebServlet("/NewListServlet")
public class NewListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newlist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("user_id");
		String password = request.getParameter("pasword");
		String password_c = request.getParameter("pasword_c");
		String username = request.getParameter("user_name");
		String date = request.getParameter("user_d");

		if (!password.equals(password_c)) {

			request.setAttribute("errMsg", "登録に失敗しました。");
			request.setAttribute("loginId",loginId);
            request.setAttribute("username",username );
           request.setAttribute("date",date);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newlist.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if (loginId.equals("") || password.equals("") || password_c.equals("") || username.equals("")
				|| date.equals("")) {

			request.setAttribute("errMsg", "登録に失敗しました。");
			request.setAttribute("loginId",loginId);
            request.setAttribute("username",username );
           request.setAttribute("date",date);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newlist.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDao userDao = new UserDao();
		User miss = userDao.miss(loginId);
		if(miss != null) {
			request.setAttribute("errMsg", "登録に失敗しました。");
			request.setAttribute("loginId",loginId);
            request.setAttribute("username",username );
           request.setAttribute("date",date);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newlist.jsp");
			dispatcher.forward(request, response);
			return;
		}


		String result= userDao.Cipher(password);
		 userDao.Insert( loginId, result,username, date);

		response.sendRedirect("UserListServlet");
	}

}
