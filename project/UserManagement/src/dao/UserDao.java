package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE name not like '%管理者%'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void Insert(String loginId, String password, String username, String date) {
		Connection conn = null;

		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			String iSQL = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			stmt = conn.prepareStatement(iSQL);
			stmt.setString(1, loginId);
			stmt.setString(2, username);
			stmt.setString(3, date);
			stmt.setString(4, password);
			int r = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public User detail(String id) {
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String SQL = "SELECT * FROM user WHERE id = ?";
			stmt = conn.prepareStatement(SQL);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				return null;
			}

			int iddata = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(iddata, loginId, name, birthDate, password, createDate, updateDate);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void Update(String name, String birthdate, String password, String id) {
		Connection conn = null;

		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			String iSQL = "UPDATE user SET name=? ,birth_date=? ,password=? ,update_date =now() WHERE id = ?";
			stmt = conn.prepareStatement(iSQL);
			stmt.setString(1, name);
			stmt.setString(2, birthdate);
			stmt.setString(3, password);
			stmt.setString(4, id);
			int r = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void UpdateNotPassword(String name, String birthdate, String id) {
		Connection conn = null;

		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			String iSQL = "UPDATE user SET name=? ,birth_date=?,update_date =now() WHERE id = ?";
			stmt = conn.prepareStatement(iSQL);
			stmt.setString(1, name);
			stmt.setString(2, birthdate);
			stmt.setString(3, id);
			int r = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void Delete(String loginid) {
		Connection conn = null;

		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			String iSQL = "DELETE FROM user WHERE login_id = ?;";
			stmt = conn.prepareStatement(iSQL);
			stmt.setString(1, loginid);

			int r = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public List<User> search(String loginId,String name,String date,String bdate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE name not like '%管理者%' ";

			if(!loginId.equals("")) {
				sql += " and login_id = '" + loginId + "'";
			}

			if(!name.equals("")) {
				sql +=" and name LIKE '%"+ name + "%'";
			}

			if(!date.equals("")) {
			sql +=" and birth_date >='" +date + "'";
			}

			if(!bdate.equals("")) {
			sql	+= "and birth_date <='" +bdate +"'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String username = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginid, username, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User miss(String loginId) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public String Cipher(String password) {
		String result="";

try {
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
		 result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
}catch(Exception e) {
	System.out.print(e);
}return result;
}

}