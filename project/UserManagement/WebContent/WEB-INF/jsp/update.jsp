<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="userlist.css">
  <title>ユーザ情報更新</title>
</head>
<body>

<!-- header -->
  <header>
  	<h1 class="headline">

    </h1>
    <ul class="nav-list">

      <li class="nav-list-item">${userInfo.name}さん </li>
      <li class="nav-list-item">ログアウト</li>
    </ul>
  </header>
     <h1>ユーザ更新</h1>
                                   <hr>
                                   <c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
                                   <div align="center">
                                    <table border="0">
                                     <form action="UpdateServlet"method="post">

   <tr>
     <th>
        ログインID    ${loginId}                                                                                                                                           </th>
      <td>
 <input type="hidden" name="id"value="${id}  "size="24">
  <input type="hidden" name="loginId"value="${loginId}  "size="24">
        </td>
        </tr>
       <tr>
        <th>
        パスワード
        </th>
        <td>
           <input type="password"name="password"value=""size="24">
           </td>
            </tr>
             <tr>
        <th>
        パスワード(確認)
        </th>
        <td>
           <input type="password"name="password_c"value=""size="24">
           </td>
            </tr>
             <tr>
        <th>
        ユーザ名
        </th>
        <td>
           <input type="text"name="name"value="${name}"size="24">
           </td>
            </tr>
              <tr>
        <th>
        生年月日
        </th>
        <td>
           <input type="text"name="date"value="${date }"size="24">
           </td>
            </tr>
           <tr>

           <tr>
            <td colspan="2">
                <input type="submit"value="更新">
               </td>
                 </tr>
 </form>
</body>
</html>