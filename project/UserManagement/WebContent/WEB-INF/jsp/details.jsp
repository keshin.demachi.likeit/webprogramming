<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<title>ユーザ詳細</title>
</head>
<body>

	<!-- header -->
	<header>
		<h1 class="headline"></h1>
		<ul class="nav-list">
			<li class="nav-list-item">${userInfo.name}さん </li>
			<li class="nav-list-item">ログアウト</li>
		</ul>
	</header>
	<h1>ユーザ情報詳細参照</h1>
	<hr>
	<div align="center">
		<table border="0">
			<tr>
				<th>ログインID ${user.loginId}</th>
				<td></td>
			</tr>
			<tr>
				<th>ユーザ名 ${user.name}</th>
				<td></td>
			</tr>
			<tr>
				<th>生年月日 ${user.birthDate }</th>
				<td></td>
			</tr>
			<tr>
				<th>登録日時 ${user.createDate }</th>
				<td></td>
			</tr>
			<tr>
				<th>更新日時 ${user.updateDate }</th>
				<td></td>
			</tr>

			<form action="UserListServlet" method="get">
				<tr>
					<td colspan="1">
						<button type="submit" value="戻る"
							class="btn btn-primary form-submit">戻る</button>
					</td>
				</tr>
			</form>
		</table>
	</div>
</body>

</html>