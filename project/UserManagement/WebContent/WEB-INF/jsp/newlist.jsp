<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<title>ユーザ一覧</title>
</head>
<body>

	<!-- header -->
	<header>
		<h1 class="headline"></h1>
		<ul class="nav-list">
			<li class="nav-list-item"></li>
			<li class="nav-list-item">${userInfo.name}さん </li>
			<li class="nav-list-item">ログアウト</li>
		</ul>
	</header>
	<h1>ユーザ新規登録</h1>
	<hr>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<table border="0">
		<div align="center">
			<th>新規登録</th>
			<tr>
				<form action="NewListServlet" method="post">
					<th>ログインID</th>
					<td><input type="text" name="user_id" value="${loginId }" size="24">
					</td>
			</tr>
			<tr>
				<th>パスワード</th>
				<td><input type="password" name="pasword" value="" size="24">
				</td>
			</tr>

			<tr>
				<th>パスワード(確認)</th>
				<td><input type="passwordc" name="pasword_c" value="" size="24">
				</td>
			</tr>
			<tr>
				<th>ユーザ名</th>
				<td><input type="username" name="user_name" value="${username}" size="24">
				</td>
			</tr>

			<tr>
				<th>生年月日</th>
				<td><input type="date" name="user_d" value="${date}" size="24">
				</td>
			</tr>
			<td colspan="2">
				<button type="submit" value="登録" class="btn btn-primary form-submit">登録</button>

				</form>

			</td>

			<form action="UserListServlet" method="get">
				<td colspan="1">
					<button type="submit" value="戻る"
						class="btn btn-primary form-submit">戻る</button>
				</td>
			</form>
	</table>